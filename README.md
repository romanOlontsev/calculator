# Calculator

The application is launched in the [`Main`](src/main/java/com/epam/learn/calculator/Main.java) class.

Expressions are entered as a string through the console. 
The result is output to the same place and to the log file. 
Exceptions are also duplicated in the log.
Use "exit" to exit the application.

Fractional numbers separated by a dot, operations of addition, subtraction, division and multiplication are supported. 
Parentheses are allowed to indicate the order of operations. 
Negative numbers enclosed in parentheses are also supported. 
Spaces entered with the expression are ignored.