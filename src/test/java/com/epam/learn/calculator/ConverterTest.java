package com.epam.learn.calculator;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class ConverterTest {

    @Test
    public void testRPNConvert() {
        List<String> parsedExpr = ReversePolishNotationConverter.convert(Arrays.asList(
                "1.234", "*", "0", "+", "4.3", "-", "44.23", "*", "5", "/", "8.9", "+", "1"));
        String expected = "[1.234, 0, *, 4.3, +, 44.23, 5, *, 8.9, /, -, 1, +]";
        Assert.assertEquals(expected, parsedExpr.toString());
    }

    @Test
    public void testRPNConvertWithParentheses() {
        List<String> parsedExpr = ReversePolishNotationConverter.convert(Arrays.asList(
                "1.234", "*", "(", "0", "+", "4.3", ")", "-", "44.23", "*", "5", "/", "(", "8.9", "+", "1", ")"));
        String expected = "[1.234, 0, 4.3, +, *, 44.23, 5, *, 8.9, 1, +, /, -]";
        Assert.assertEquals(expected, parsedExpr.toString());
    }

    @Test
    public void testRPNConvertWithNegativeNumbers() {
        List<String> parsedExpr = ReversePolishNotationConverter.convert(Arrays.asList(
                "1.234", "*", "(", "0", "+", "(", "-4.3", ")", ")", "-", "44.23", "*", "5", "/",
                "(", "(", "-8.9", ")", "+", "1", ")"));
        String expected = "[1.234, 0, -4.3, +, *, 44.23, 5, *, -8.9, 1, +, /, -]";
        Assert.assertEquals(expected, parsedExpr.toString());
    }
}
