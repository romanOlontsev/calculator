package com.epam.learn.calculator;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CalculatorTest {

    @Rule
    public final ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void testDivisionByZero() {
        String expression = "1.234*0+(4.3-44.23)*5/0+1";
        Assert.assertNull(Calculator.calculate(expression));
    }

    @Test
    public void testCalculateExpWithoutParentheses() {
        String expression = "12.32*234.21123-2.323/99.00/66.77+3.4465";
        Assert.assertEquals("2888.9285022", Calculator.calculate(expression));
    }

    @Test
    public void testCalculateExpWithParentheses() {
        String expression = "(12.32*234.21123-(2.323/99.00))/66.77+3.4465";
        Assert.assertEquals("46.6614002", Calculator.calculate(expression));
    }

    @Test
    public void testCalculateExpWithNegativeNumbers() {
        String expression = "(12.32*234.21123-(2.323/99.00))/66.77+(-3.4465)";
        Assert.assertEquals("39.7684002", Calculator.calculate(expression));
    }
}