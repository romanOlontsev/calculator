package com.epam.learn.calculator;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class ParserTest {

    @Test
    public void testParsingNullExpression() {
        List<String> expression = Parser.parsing(null);
        String expected = "[]";
        Assert.assertEquals(expected, expression.toString());
    }

    @Test
    public void testParsingZeroLengthExpression() {
        List<String> expressionRPN = Parser.parsing("");
        String expected = "[]";
        Assert.assertEquals(expected, expressionRPN.toString());
    }

    @Test
    public void testParsingExpression() {
        List<String> expressionRPN = Parser.parsing("1.234*0+4.3-44.23*5/8.9+1");
        String expected = "[1.234, *, 0, +, 4.3, -, 44.23, *, 5, /, 8.9, +, 1]";
        Assert.assertEquals(expected, expressionRPN.toString());
    }

    @Test
    public void testParsingExpressionWithParentheses() {
        List<String> expressionRPN = Parser.parsing("1.234*(0+4.3)-44.23*5/(8.9+1)");
        String expected = "[1.234, *, (, 0, +, 4.3, ), -, 44.23, *, 5, /, (, 8.9, +, 1, )]";
        Assert.assertEquals(expected, expressionRPN.toString());
    }

    @Test
    public void testParsingExpressionWithNegativeNumbers() {
        List<String> expressionRPN = Parser.parsing("1.234*(0+(-4.3))-44.23*5/((-8.9)+1)");
        String expected = "[1.234, *, (, 0, +, (, -4.3, ), ), -, 44.23, *, 5, /, (, (, -8.9, ), +, 1, )]";
        Assert.assertEquals(expected, expressionRPN.toString());
    }
}
