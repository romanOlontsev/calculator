package com.epam.learn.calculator;

import com.epam.learn.calculator.exceptions.InvalidInputExpressionException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ValidationTest {

    @Rule
    public final ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void testInputCharacterLetter() throws IOException {
        List<String> expression = Arrays.asList("5", "/", "s", "+", "1");
        String expectedMessage = "\"s\" - invalid expression symbol";
        expectedInvalidExpressionException(expression, expectedMessage);
    }

    @Test
    public void testInputCharacterSymbol() throws IOException {
        List<String> expression = Arrays.asList(
                "1.2", "+", "9.90", "*", "(", "90", "+", "2.22", ")", "*", "0", "+",
                "(", "(", "-5", ")", "/", "(", "-0.234","$", ")", ")", "+", "1");
        String expectedMessage = "\"$\" - invalid expression symbol";
        expectedInvalidExpressionException(expression, expectedMessage);
    }

    @Test
    public void testInputCharacterComma() throws IOException {
        List<String> expression = Arrays.asList(
                "(","67.2","+","12.3","*","(","188,754","/","2344.323",")","+","12",")","/","23.4");
        String expectedMessage = "\",\" - invalid expression symbol";
        expectedInvalidExpressionException(expression, expectedMessage);
    }

    @Test
    public void testUnnecessaryOperator() throws IOException {
        List<String> expression = Arrays.asList("5", "/", "2", "+");
        String expectedMessage = "1 or more numbers are missing in the expression";
        expectedInvalidExpressionException(expression, expectedMessage);
    }

    @Test
    public void testOnlyParenthesis() throws IOException {
        List<String> expression = Arrays.asList("(",")");
        String expectedMessage = "The expression is missing numbers.";
        expectedInvalidExpressionException(expression, expectedMessage);
    }


    @Test
    public void testMissingLeftParenthesis() throws IOException {
        List<String> expression = Arrays.asList(
                "(","4423.3231","-","77644.123",")","+","777.44","-","(","(","-12",")","*","234.3",")",")");
        String expectedMessage = "Missing left parenthesis.";
        expectedInvalidExpressionException(expression, expectedMessage);
    }

    @Test
    public void testMissingRightParenthesis() throws IOException {
        List<String> expression = Arrays.asList(
                "(","-14.4",")","+","(","234","+","2.33",")","/","266.5","-","(","(","-0.234",")","*","555.5");
        String expectedMessage = "Missing right parenthesis.";
        expectedInvalidExpressionException(expression, expectedMessage);
    }

    @Test
    public void testRepeatingOperatorDivision() throws IOException {
        List<String> expression = Arrays.asList(
                "(","(","-999.02",")","/","23.4","+","3",")","*","(","/","234.4","*","33.4",")");
        String expectedMessage = "Operator repeat: \"/\" after \"*\".";
        expectedInvalidExpressionException(expression, expectedMessage);
    }

    @Test
    public void testRepeatingOperatorPoint() throws IOException {
        List<String> expression = Arrays.asList("(", "-2.1", "+", "2..3", ")", "*", "23.4");
        String expectedMessage = "Operator repeat: \".\" after \".\".";
        expectedInvalidExpressionException(expression, expectedMessage);
    }

    @Test
    public void testRepeatingOperatorSubtract() throws IOException {
        List<String> expression = Arrays.asList("55.4", "-", "(", "-23.3", ")", "*", "34.423", "/", "(", "-23", ")");
        Validator.checkInputExpression(expression);
        Assert.assertTrue(true);
    }

    private void expectedInvalidExpressionException(List<String> expression, String expectedMessage)
            throws InvalidInputExpressionException {
        exceptionRule.expect(InvalidInputExpressionException.class);
        exceptionRule.expectMessage(expectedMessage);
        Validator.checkInputExpression(expression);
    }
}