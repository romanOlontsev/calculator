package com.epam.learn.calculator;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class ReversePolishNotationConverter {
    public static List<String> convert(List<String> inputExpression) {
        List<String> resultArray = new ArrayList<>(inputExpression.size());
        Deque<String> operatorsStack = new LinkedList<>();
        for (String token : inputExpression) {
            if (isDigit(token)) {
                resultArray.add(token);
            } else if (token.equals("(")) {
                operatorsStack.push(token);
            } else if (token.equals(")")) {
                while (!operatorsStack.peek().equals("(")) {
                    resultArray.add(operatorsStack.pop());
                }
                operatorsStack.pop();
            } else {
                while (!operatorsStack.isEmpty() && getOperatorPriority(operatorsStack.peek()) >= getOperatorPriority(token)) {
                    resultArray.add(operatorsStack.pop());
                }
                operatorsStack.push(token);
            }
        }
        while (!operatorsStack.isEmpty()) {
            resultArray.add(operatorsStack.pop());
        }
        return resultArray;
    }

    private static boolean isDigit(String token) {
        return Character.isDigit(token.charAt(0));
    }

    private static int getOperatorPriority(String operator) {
        if (operator.equals("(")) {
            return 0;
        } else if (operator.equals("+") || operator.equals("-")) {
            return 1;
        } else {
            return 2;
        }
    }
}

