package com.epam.learn.calculator;

import com.epam.learn.calculator.exceptions.InvalidInputExpressionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class Calculator {
    static Logger logger = LogManager.getLogger();

    public static String calculate(String expression) {
        List<String> parsedExpression = Parser.parsing(expression);
        try {
            Validator.checkInputExpression(parsedExpression);
        } catch (InvalidInputExpressionException exception) {
            logger.debug(exception.getClass(), exception);
            logger.info("Incorrect expression input. " + exception.getMessage()+"\nTry again.\n");
            return null;
        }
        List<String> tokens = ReversePolishNotationConverter.convert(parsedExpression);
        Deque<BigDecimal> stack = new LinkedList<>();
        String operators = "+-*/";
        String stackOperation;
        BigDecimal x, y;
        for (String token : tokens) {
            if (!operators.contains(token)) {
                stack.push(new BigDecimal(token));
                continue;
            } else {
                stackOperation = token;
            }
            switch (stackOperation) {
                case "+":
                    x = stack.pop();
                    y = stack.pop();
                    stack.push(x.add(y));
                    break;
                case "-":
                    x = stack.pop();
                    y = stack.pop();
                    stack.push(y.subtract(x));
                    break;
                case "*":
                    x = stack.pop();
                    y = stack.pop();
                    stack.push(x.multiply(y));
                    break;
                case "/":
                    x = stack.pop();
                    y = stack.pop();
                    if (x.equals(BigDecimal.ZERO)) {
                        logger.debug("RuntimeException - division by zero");
                        logger.info("The expression contains division by zero.\nTry again.\n");
                        return null;
                    }
                    stack.push(y.divide(x, 9, RoundingMode.HALF_UP));
                    break;
            }
        }
        return stack.pop()
                .setScale(7, RoundingMode.HALF_UP)
                .stripTrailingZeros()
                .toPlainString();
    }
}