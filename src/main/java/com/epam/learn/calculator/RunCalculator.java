package com.epam.learn.calculator;

public class RunCalculator {
    public static void run() {
        String inputExpression;
        String calculationResult;
        while (true) {
            inputExpression = Console.inputExpression();
            if (inputExpression.equals("exit")) {
                return;
            }
            calculationResult = Calculator.calculate(inputExpression);
            if (calculationResult == (null)) {
                continue;
            }
            Console.print(calculationResult);
        }
    }
}
