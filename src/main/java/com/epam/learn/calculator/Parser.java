package com.epam.learn.calculator;

import java.util.ArrayList;
import java.util.List;

public class Parser {
    public static List<String> parsing(String inputExpression) {
        if (inputExpression == null || inputExpression.length() == 0) {
            return new ArrayList<>(0);
        }
        String inputStringWithoutSpaces = inputExpression.replaceAll("\\s+", "");
        String[] splitInputString = inputStringWithoutSpaces.split(
                "(?<=[\\d.])(?=[^\\d.])|(?<=[^\\d.])(?=[\\d.])|(?=[()])|(?<=[()])");
        List<String> symbolsList = new ArrayList<>();

        StringBuilder compoundOperator = new StringBuilder();
        boolean isThereMinus = false;
        for (int i = 0; i < splitInputString.length; i++) {
            String currentElement = splitInputString[i];
            if (i >= 1 && splitInputString[i - 1].equals("(") && currentElement.equals("-")) {
                isThereMinus = true;
            } else {
                if (isThereMinus) {
                    compoundOperator.append("-").append(currentElement);
                    symbolsList.add(compoundOperator.toString());
                    compoundOperator.setLength(0);
                    isThereMinus = false;
                } else {
                    symbolsList.add(String.valueOf(currentElement));
                }
            }
        }
        return symbolsList;
    }
}
