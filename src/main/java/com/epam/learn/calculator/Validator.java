package com.epam.learn.calculator;

import com.epam.learn.calculator.exceptions.InvalidInputExpressionException;

import java.util.List;

public class Validator {
    private static final String supportedOperators = "*/+-.";
    private static int countDigits;
    private static int countOperators;
    private static int countLeftParenthesis;
    private static int countRightParenthesis;
    private static char repeatingOperatorSymbol;
    private static boolean repeatingOperator;
    private static boolean hasDigit;
    private static boolean previousIsParenthesis;

    public static void checkInputExpression(List<String> inputExpression) throws InvalidInputExpressionException {
        repeatingOperatorSymbol = '\u0000';
        repeatingOperator = false;
        hasDigit = false;
        previousIsParenthesis = false;
        countLeftParenthesis = 0;
        countRightParenthesis = 0;
        countDigits = 0;
        countOperators = 0;

        for (String currentElement : inputExpression) {
            countDigitsAndOperatorsNumber(currentElement);
            checkCharByChar(currentElement);
        }

        checkCorrectParenthesesInput(countLeftParenthesis, countRightParenthesis);
        checkDigitsPresence(hasDigit);
        compareDigitsAndOperatorsNumber(countDigits, countOperators);
    }

    private static void checkCharByChar(String currentElement) throws InvalidInputExpressionException {
        char[] inputExpressionCharArray = currentElement.toCharArray();
        for (char currentCharacter : inputExpressionCharArray) {
            checkCharacter(currentCharacter);
        }
    }

    private static void checkCharacter(char currentCharacter) throws InvalidInputExpressionException {
        if (isDigit(currentCharacter) || isOperator(currentCharacter)
                || isParenthesis(currentCharacter)) {
            charIdentification(currentCharacter);
        } else {
            throw new InvalidInputExpressionException("\"" + currentCharacter + "\" - invalid expression symbol.");
        }
    }

    private static void charIdentification(char currentCharacter) throws InvalidInputExpressionException {
        boolean isNegativeNumber = false;
        if (isDigit(currentCharacter)) {
            repeatingOperator = false;
            hasDigit = true;
        } else if (isOperator(currentCharacter)) {
            if (currentCharacter == '-' && previousIsParenthesis) {
                isNegativeNumber = true;
            }
            if (repeatingOperator && !isNegativeNumber) {
                throw new InvalidInputExpressionException(
                        "Operator repeat: \"" + currentCharacter + "\" after \"" +
                                repeatingOperatorSymbol + "\".");
            }
            repeatingOperatorSymbol = currentCharacter;
            repeatingOperator = true;
        } else if (isParenthesis(currentCharacter)) {
            if (currentCharacter == '(') {
                countLeftParenthesis++;
                previousIsParenthesis = true;
            } else {
                countRightParenthesis++;
            }
        }
    }

    private static void countDigitsAndOperatorsNumber(String currentElement) {
        if (isNumeric(currentElement)) {
            countDigits++;
        } else if (supportedOperators.contains(currentElement)) {
            countOperators++;
        }
    }

    private static void compareDigitsAndOperatorsNumber(int countDigits, int countOperators) throws InvalidInputExpressionException {
        if (countDigits <= countOperators) {
            throw new InvalidInputExpressionException("1 or more numbers are missing in the expression.");
        }
    }

    private static void checkDigitsPresence(boolean hasDigit) throws InvalidInputExpressionException {
        if (!hasDigit) {
            throw new InvalidInputExpressionException("The expression is missing numbers.");
        }
    }

    private static void checkCorrectParenthesesInput(int countLeftParenthesis, int countRightParenthesis) throws InvalidInputExpressionException {
        if (countLeftParenthesis != countRightParenthesis) {
            String exceptionMessage;
            if (countLeftParenthesis > countRightParenthesis) {
                exceptionMessage = "Missing right parenthesis.";
            } else {
                exceptionMessage = "Missing left parenthesis.";
            }
            throw new InvalidInputExpressionException(exceptionMessage);
        }
    }

    private static boolean isOperator(char currentElement) {
        return supportedOperators.contains(String.valueOf(currentElement));
    }

    private static boolean isParenthesis(char currentElement) {
        return "()".contains(String.valueOf(currentElement));
    }

    private static boolean isDigit(char token) {
        return Character.isDigit(token);
    }

    public static boolean isNumeric(String currentElement) {
        return currentElement.matches("-?\\d+(\\.\\d+)?");
    }
}