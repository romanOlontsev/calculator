package com.epam.learn.calculator.exceptions;

import java.io.IOException;

public class InvalidInputExpressionException extends IOException {
    public InvalidInputExpressionException() {
        super();
    }

    public InvalidInputExpressionException(String message) {
        super(message);
    }

    public InvalidInputExpressionException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidInputExpressionException(Throwable cause) {
        super(cause);
    }
}
