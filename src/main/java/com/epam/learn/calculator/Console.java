package com.epam.learn.calculator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Console {
    static Logger logger = LogManager.getLogger();

    public static String inputExpression() {
        logger.info("Enter your expression:");
        Scanner scanner = new Scanner(System.in);
        String expression = scanner.nextLine();
        if (expression.equals("exit")) {
            logger.debug("exit");
            return expression;
        }
        logger.info("Your expression: " + expression);
        return expression;
    }

    public static void print(String result) {
        logger.info("Result: " + result + "\n");
    }
}
